﻿1714 McCreary Avenue, Owensboro, KY 42301 (+1)(033) 225 8776 bman.blount02325@  gmail.com
C Bryan Blount
Professional
Highlights
Education
I am currently the Senior Director for Information Services and Resources for Kentucky Wesleyan
College. The Senior Director for Information Services and Resources (SDISR) serves as the chief
information officer and is responsible for leading the integration of information resources, library, and
technology activities to support the academic and operational goals of the college. The SDISR has
responsibility for all information services and activities including the library, technology infrastructure,
administrative systems, academic and instructional technology, telephone systems, and user support. I
provide leadership, direction, and assistance in the formulation and development of long—range plans
related to the utilization of information technology to support academic programs effectively.
Additionally, I lead and direct the development and implementation ofthe college’s security policy,
standards, guidelines and procedures. Some of the more extensive projects I have led to date:
0 CAMS Enterprise data conversion and implementation
0 Disaster Recovery/Business Continuity implementation
0 Learning Management System conversion
Summary
0 Electronic Records Management policy and procedures implementation
I began my Kentucky Wesleyan College career as a computer systems analyst/administrator (job titled as
Software Information Coordinator). As Software Information Coordinator, I was responsible for
providing assistance to KWC faculty and staff in maximizing the use ofthe student information system,
CAMS Enterprise. As the direct liaison to the Center for Engaged Teaching & Learning (CETL), I was
responsible for translating pedagogical change into technology—assisted engaged—learning environments.
Working as the Moodle administrator, I have been responsible for the daily health of the college's
Learning Management System. My responsibilities have included implementing patches, upgrades,
service packs, hotfixes, and other routine maintenance to ensure the highest possible uptime and
reliability of the LMS service. Additionally, I assisted Information Technology Services in the database
administration ofthe campus software system.
I am the CEO for BEST (Blount Educational Services and Technologies) an educational consulting firm. My most
recent clients have been:
1. WestEd: For this project, Partnership for Assessment for College and Careers (PARCC), my primary
responsibility has been writing online, interactive mathematics assessment items for grades 3
through 8 as well as for Algebra I courses.
1714 McCreary Avenue, Owensboro, KY 42 301 270  302  0655 bman.blount02325@  gmail.co. in
C Bryan Blount
2. Connections Education: | contracted with Connections as an item editor and writer for their
randomized mathematics assessment project, and for the Texas alternative text AP Calculus e—text
project.
3. WiseWire (formally Words and Numbers): This project is a partnership with Rice University's
OpenStax initiative. I have contributed and edited for both the OpenStax Statistics textbook as well
as the OpenStax PreCaIcqus textbook.
I have written and edited over 3,000 assessment items using research—based item writing
methodology.
My previous positions were directing my district’s STEM engineering program and working as an analyst with
school—level data. My district focused on driving student improvement along with teacher growth through
systematic data analysis. We established data teams within each school and across the district that were
trained in root cause analysis and action research. My roles were focused on helping to design mixed methods
research that were based on data—driven performance—gap identifications, analyzing the results using
computer based tool such as SPSS, and drafting executive—level summaries intended for school and district
administrators as well as site based councils.
In our first year of implementing Project Lead the Way in grades 5 through 12, I was responsible for:
1. Developing a 5—year strategic plan that included a scope and sequence incorporating grades 5
through 12 with technology tracks available for dual credit and certification possibilities.
2. Implementing community and business partnerships with the purpose of establishing and funding
learning grants, developing a career—mentoring program, and implementing an internship
pathway.
grants totaling $38,000 and management ofthe project budgets attached with the grants
4. Designing and implementing curriculum for non—PLTW engineering classes incorporating the Next
Generation Science Standards.
5. Designing and implementing engineering curriculum that encompassed Common Core ELA
standards. For example, I developed units that focused on research projects requiring students to
gather information pertaining to: the grand engineering challenges, the four types of robotics and
their impact on the geo—human environment, engineering careers, and technological artifacts. Not
only did students research these topics; they also communicated their findings with the spoken
and written word to audiences ranging from peers to industry professionals.
5”
I have been the lead educator responsible for developing an integrated advanced mathematics program of
instruction that included traditional and digital curriculum designed to meet Kentucky standards for middle
grades and high school mathematics. Curriculum design elements that I have incorporated include Gardner’s
Multiple Intelligences theories along with constructivist learning theories used to address the individual
student affective and cognitive needs. Classes taught include: 1— pre—algebra with a focus on fluency in real
number computation and basic modeling of linear equations and inequalities.2— high school algebra |, 3—
analytical geometry, 4—trigonometry and pre—calculus, and 5— high school algebra ||.
1714 McCreary Avenue, Owensboro, KY 42301 270-302-0655 bman.blount02325@gmail.com
C Bryan Blount
I have been the department chair responsible for managing a 6—teacher project focused on a curriculum
transition toward common learning goals that has focused on individualized data—driven formative—
assessment curriculum design. In addition to being an academic department chair, I have served as one
of three teacher representatives on the school’s site based decision committee. These state mandated
committees have oversight and final decision—making powers for all areas ofthe school’s operations
except personnel terminations.
As well as a K—12 educator, I am a college professor responsible for teaching college algebra, trigonometry,
calculus, and non—calculus based statistics. I have designed curriculum for traditional classes, hybrid classes,
and distance—learning classes for both traditional as well as non—traditional college students that incorporate
the ADDIE model. Along with the ADDIE system, my main instructional design elements have included
incorporating possible life—experiences into prior—knowledge triggers as well as framing most learning
outcomes within project—based—learning (experiential learning) as well as team—based problem analysis. I am a
proficient user and have experience with multiple Learning Management Systems such as Canvas, Blackboard,
Moodle, MathXL, and MathLab.
Quality Engineering
I have been a HACCP (Hazard Analysis Critical Control Point system) analyst determining where hazards might
occur in the food production process and designing and implementing stringent procedures to prevent the
hazards from occurring. My design and implementation procedures were based on statistical process control
methodology.
Additionally, I have served as an ISO 9000 quality engineer given oversight of documenting a set of procedures
that covered all key processes in the business, monitoring processes to ensure they are effective, keeping
adequate records, checking output for defects by utilizing SPC analysis as well as AQL sampling techniques,
with appropriate and corrective action where necessary, regularly reviewing individual processes and the
quality system itself for effectiveness, and facilitating continual improvement. I am proficient with statistical
software (Mini—Tab)
Industrial Engineering
As an Industrial Engineer, I was primarily concerned with designing, developing, testing, and evaluating
integrated systems for managing industrial production and distribution processes including human work
factors, quality control, inventory control, logistics and material flow, cost analysis, and production
coordination. As part of my management duties, I was responsible for recruitment, training, and
supervision of all industrial engineering technicians as well as primary budgetary authorship and
secondary oversight of the plant’s five million dollar operating budget. Finally, I have been trained as a
Kaizen and Lean Engineer responsible for identifying and implementing continual, incremental quality
improvements. Tools used in the process included variable control charts, TPS, 55, root cause analysis
along with solid statistical analysis.
C Bryan Blount
1714 McCreary Avenue, Owensboro, KY 42301 270-302-0655 bman.blount02325@gmail.com
Employment
History
Senior Director for Information Kentucky Wesleyan College
Services and Resources
Systems Analyst
Teacher
Adjunct Instructor
Adjunct Instructor
Quality Assurance
Technician
Industrial Engineer
Industrial Technician
Education
Master of Arts
Mathematics
Bachelors of Science
Mathematics
Owensboro, KY
Kentucky Wesleyan College
Owensboro, KY
Owensboro Public Schools
Owensboro, KY
Kentucky Wesleyan College
Owensboro, KY
Daymar College
Owensboro, KY
Field Packing
Owensboro, KY
UniFirst, Owensboro, KY
United Parcel Service
Nashville, TN
Western Kentucky University
Kentucky Wesleyan College
Jan 2015 — Present
2006 - 2015
Aug 2010 — Present
Aug 2008 — Aug 2010
2003 - 2005
2000 - 2003
1996 - 2000
August 2011
December 2005