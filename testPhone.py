import re
import numpy as np
phoneRegex = re.compile(r'(\d\d\d\W*)?\d\d\d\W*\d\d\d\d')
phone_list = []

with open('E:/python/text.txt', 'rt', encoding="utf8") as myfile:
    for myline in myfile:
        phn = phoneRegex.search(myline)
        if phn:
            strip_phn = re.sub('[^0-9]', '', phn.group())
            if len(strip_phn) == 10:
                #clean_num = re.sub(r"[^0-9]+", "/", phn.group())
                phone_list.append(strip_phn)

phone_list_unique = np.array(np.unique(phone_list))
print(phone_list_unique)
