import re
import pandas as pd

resume_attr = ['objective', 'summary', 'recognized for',
               'education', 'certificate']
df = pd.read_csv('resume.csv')
resume_csv = df.loc[0][9]
pos = dict()


def getPos():
    for attr in resume_attr:
        pattern = re.compile(attr)
        for m in pattern.finditer(resume_csv.lower()):
            pos[m.start()] = m.group().strip()
    # print(pos)
    return sorted(pos.items())


class getExtractedText:
    def __init__(self):
        self.pos = getPos()
        self.index = 0
        self.endpos = 0
        self.attr_list = [item[1] for item in self.pos]
        print('Sorted =====', self.pos)

    def objective(self):
        if len(self.pos) > 0:
            startpos = self.pos[0][0]
        else:
            startpos = 0
        for k, v in self.pos:
            if v != 'objective':
                self.endpos = k
                break
        self.index = [item[0] for item in self.pos].index(self.endpos)
        if 'objective' in self.attr_list:
            if startpos == self.endpos:
                st = (resume_csv[startpos:].strip())
            else:
                st = (resume_csv[startpos:self.endpos].strip())
            st = st.replace('objective', '')
            st = re.sub('[:;|]', '', st)
            print('Objective=============', st)
            # print('{} {}'.format(startpos, self.endpos))
            # print('oooo', self.index)

    def summary(self):
        startpos = self.endpos
        for k, v in self.pos[self.index:]:
            if v != 'summary':
                self.endpos = k
                break
        self.index = [item[0] for item in self.pos].index(self.endpos)
        if 'summary' in self.attr_list:
            if startpos == self.endpos:
                st = (resume_csv[startpos:].strip())
            else:
                st = (resume_csv[startpos:self.endpos].strip())
            st = st.replace('summary', '')
            st = re.sub('[:;|]', '', st)
            print('Summary=============', st)

    def skill(self):
        startpos = self.endpos
        for k, v in self.pos[self.index:]:
            if v != 'recognized for':
                self.endpos = k
                break
        self.index = [item[0] for item in self.pos].index(self.endpos)
        if 'recognized for' in self.attr_list:
            if startpos == self.endpos:
                st = (resume_csv[startpos:].strip())
            else:
                st = (resume_csv[startpos:self.endpos].strip())
            st = st.replace('recognized for', '')
            st = re.sub('[:;|]', '', st)
            print('Skill=============', st)

    def education(self):
        startpos = self.endpos
        for k, v in self.pos[self.index:]:
            if v != 'education':
                self.endpos = k
                break
        self.index = [item[0] for item in self.pos].index(self.endpos)
        if 'education' in self.attr_list:
            if startpos == self.endpos:
                st = (resume_csv[startpos:].strip())
            else:
                st = (resume_csv[startpos:self.endpos].strip())
            st = st.replace('education', '')
            st = re.sub('[:;|]', '', st)
            print('Education=============', st)

    def certificate(self):
        startpos = self.endpos
        for k, v in self.pos[self.index:]:
            if v != 'certificate':
                self.endpos = k
                break
        self.index = [item[0] for item in self.pos].index(self.endpos)
        if 'certificate' in self.attr_list:
            if startpos == self.endpos:
                st = (resume_csv[startpos:].strip())
            else:
                st = (resume_csv[startpos:self.endpos].strip())
            st = st.replace('certificate', '')
            st = re.sub(r'[:;|]', '', st)
            print('Certificate=============', st)


txt = getExtractedText()
txt.objective()
txt.summary()
txt.skill()
txt.education()
txt.certificate()
