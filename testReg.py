import re

# s = '18/2/2006 In-design, Acrobat, light room'
# s = s.translate(s.maketrans('','',string.punctuation))
# print(s)
# print(s.encode('ascii', 'ignore'))

pattern=re.compile(r'[\w\.-]+@[\w\.-]+')
st='disposable.style.email.with+symbol@example.com'
email=pattern.search(st)
if email:
    print('Email found')
else:
    print('Email not found')

