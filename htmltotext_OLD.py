import re
import codecs
from html2text import HTML2Text
from fpdf import FPDF
import os
import pdfkit

from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from io import StringIO


base_dir = 'D:/virtual_env/'
html_path = base_dir + 'html/'
pdf_path = base_dir + 'pdf/'


def htmlToText():
    file_name_lst = os.listdir(html_path)
    for item in file_name_lst:
        name = item.split('.')[0]
        html_file = html_path + item
        f = codecs.open(html_file, 'r')
        st_html = f.read()
        f.close()
        htmlToPdftoText(st_html, name)


def textToPdf(html, name):
    pass
    # parser = HTML2Text()
    # parser.wrap_links = False
    # parser.skip_internal_links = True
    # parser.inline_links = True
    # parser.ignore_anchors = True
    # parser.ignore_images = True
    # parser.ignore_emphasis = True
    # parser.ignore_links = True
    # text = parser.handle(html)
    # text = text.replace('*', '')
    # pdf = FPDF()
    # pdf.add_page()
    # pdf.set_font("Arial", size=10)
    # pdf.multi_cell(0, 5, text, align="L")
    # pdf.output(pdf_path+name+'.pdf')
    # print(pdf_path+name+'.pdf')


def htmlToPdftoText(html, name):
    all_ascii = ''.join(char for char in html if ord(char) < 128)
    all_ascii = all_ascii.replace('?', '')
    all_ascii = all_ascii.replace('"', '')
    all_ascii = re.sub(' +', ' ', all_ascii)
    pdfkit.from_string(all_ascii, pdf_path+name+'.pdf')

    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    #codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, laparams=laparams)
    fp = open(pdf_path+name+'.pdf', 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    # password = ""
    # maxpages = 0
    caching = True
    pagenos = set()

    for page in PDFPage.get_pages(fp, pagenos, caching=caching, check_extractable=True):
        interpreter.process_page(page)

    text = retstr.getvalue()
    fp.close()
    device.close()
    retstr.close()

    pdf = FPDF()
    pdf.add_page()
    pdf.set_font("Arial", size=10)
    pdf.multi_cell(0, 5, text)
    pdf.output(pdf_path+name+'.pdf')


htmlToText()
